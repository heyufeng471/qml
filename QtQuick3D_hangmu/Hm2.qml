import QtQuick 2.15
import QtQuick3D 1.15

Node {
    id: hm2_obj


    Model {
        id: pCube1
        source: "meshes/pCube1.mesh"

        DefaultMaterial {
            id: blinn1SG_material
            diffuseColor: "#ff949494"
        }
        materials: [
            blinn1SG_material
        ]
    }


    Model {
        id: pCube3
        source: "meshes/pCube3.mesh"

        DefaultMaterial {
            id: initialShadingGroup_material
            diffuseColor: "#ff424245"
        }

        DefaultMaterial {
            id: blinn2SG_material
            diffuseColor: "#ffff0000"
        }
        materials: [
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            blinn2SG_material,
            blinn1SG_material,
            blinn2SG_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            blinn2SG_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            blinn2SG_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            blinn2SG_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            blinn2SG_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            blinn2SG_material,
            blinn1SG_material,
            blinn2SG_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn2SG_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            blinn2SG_material,
            blinn1SG_material,
            blinn2SG_material,
            blinn1SG_material,
            blinn2SG_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            blinn2SG_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn2SG_material,
            blinn1SG_material,
            blinn2SG_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn2SG_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            blinn2SG_material,
            blinn1SG_material,
            blinn2SG_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn2SG_material,
            blinn1SG_material,
            blinn2SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            blinn2SG_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            blinn2SG_material,
            blinn1SG_material,
            blinn2SG_material,
            blinn1SG_material,
            blinn2SG_material,
            blinn1SG_material,
            blinn2SG_material,
            blinn1SG_material,
            blinn2SG_material,
            blinn1SG_material,
            blinn2SG_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            blinn2SG_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn2SG_material,
            blinn1SG_material,
            blinn2SG_material,
            blinn1SG_material
        ]
    }


    Model {
        id: pCube4
        source: "meshes/pCube4.mesh"
        materials: [
            blinn1SG_material
        ]
    }


    Model {
        id: pCube7
        source: "meshes/pCube7.mesh"
        materials: [
            blinn1SG_material
        ]
    }


    Model {
        id: pCube8
        source: "meshes/pCube8.mesh"
        materials: [
            blinn1SG_material
        ]
    }


    Model {
        id: pCube9
        source: "meshes/pCube9.mesh"
        materials: [
            blinn1SG_material,
            initialShadingGroup_material
        ]
    }


    Model {
        id: pSphere1
        source: "meshes/pSphere1.mesh"
        materials: [
            blinn2SG_material
        ]
    }


    Model {
        id: pCube10
        source: "meshes/pCube10.mesh"
        materials: [
            blinn1SG_material
        ]
    }


    Model {
        id: pCube11
        source: "meshes/pCube11.mesh"
        materials: [
            blinn1SG_material
        ]
    }


    Model {
        id: pCube12
        source: "meshes/pCube12.mesh"
        materials: [
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material
        ]
    }


    Model {
        id: pCube15
        source: "meshes/pCube15.mesh"
        materials: [
            blinn1SG_material
        ]
    }


    Model {
        id: pCube16
        source: "meshes/pCube16.mesh"
        materials: [
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material
        ]
    }


    Model {
        id: pCube17
        source: "meshes/pCube17.mesh"
        materials: [
            blinn1SG_material
        ]
    }


    Model {
        id: pCylinder3
        source: "meshes/pCylinder3.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube71
        source: "meshes/pCube71.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube72
        source: "meshes/pCube72.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: polySurface3_polySurface5
        source: "meshes/polySurface3_polySurface5.mesh"
        materials: [
            blinn1SG_material,
            initialShadingGroup_material
        ]
    }


    Model {
        id: polySurface3_polySurface5_1
        source: "meshes/polySurface3_polySurface5_1.mesh"
        materials: [
            blinn1SG_material,
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube74
        source: "meshes/pCube74.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pSphere2_pSphere3
        source: "meshes/pSphere2_pSphere3.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pSphere2_pSphere3_1
        source: "meshes/pSphere2_pSphere3_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube84
        source: "meshes/pCube84.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube85
        source: "meshes/pCube85.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube86
        source: "meshes/pCube86.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube87
        source: "meshes/pCube87.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube88
        source: "meshes/pCube88.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube89
        source: "meshes/pCube89.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder4_pCylinder6
        source: "meshes/pCylinder4_pCylinder6.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder5_pCylinder7
        source: "meshes/pCylinder5_pCylinder7.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pSphere4_pSphere5
        source: "meshes/pSphere4_pSphere5.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder4_pCylinder6_1
        source: "meshes/pCylinder4_pCylinder6_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pSphere4_pSphere5_1
        source: "meshes/pSphere4_pSphere5_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder5_pCylinder7_1
        source: "meshes/pCylinder5_pCylinder7_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube92
        source: "meshes/pCube92.mesh"
        materials: [
            blinn1SG_material
        ]
    }


    Model {
        id: pCube93
        source: "meshes/pCube93.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pSphere6
        source: "meshes/pSphere6.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube94
        source: "meshes/pCube94.mesh"
        materials: [
            blinn1SG_material
        ]
    }


    Model {
        id: pCube95
        source: "meshes/pCube95.mesh"
        materials: [
            blinn1SG_material
        ]
    }


    Model {
        id: pCube96
        source: "meshes/pCube96.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube97_pCube128
        source: "meshes/pCube97_pCube128.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube98_pCube113
        source: "meshes/pCube98_pCube113.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube99_pCube122
        source: "meshes/pCube99_pCube122.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube100_pCube121
        source: "meshes/pCube100_pCube121.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube101_pCube126
        source: "meshes/pCube101_pCube126.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube102_pCube123
        source: "meshes/pCube102_pCube123.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube103_pCube117
        source: "meshes/pCube103_pCube117.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube104_pCube116
        source: "meshes/pCube104_pCube116.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube105_pCube120
        source: "meshes/pCube105_pCube120.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube106_pCube118
        source: "meshes/pCube106_pCube118.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube107_pCube119
        source: "meshes/pCube107_pCube119.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube108_pCube127
        source: "meshes/pCube108_pCube127.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube109_pCube124
        source: "meshes/pCube109_pCube124.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube110_pCube125
        source: "meshes/pCube110_pCube125.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube111_pCube114
        source: "meshes/pCube111_pCube114.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube112_pCube115
        source: "meshes/pCube112_pCube115.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube98_pCube113_1
        source: "meshes/pCube98_pCube113_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube111_pCube114_1
        source: "meshes/pCube111_pCube114_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube112_pCube115_1
        source: "meshes/pCube112_pCube115_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube104_pCube116_1
        source: "meshes/pCube104_pCube116_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube103_pCube117_1
        source: "meshes/pCube103_pCube117_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube106_pCube118_1
        source: "meshes/pCube106_pCube118_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube107_pCube119_1
        source: "meshes/pCube107_pCube119_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube105_pCube120_1
        source: "meshes/pCube105_pCube120_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube100_pCube121_1
        source: "meshes/pCube100_pCube121_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube99_pCube122_1
        source: "meshes/pCube99_pCube122_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube102_pCube123_1
        source: "meshes/pCube102_pCube123_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube109_pCube124_1
        source: "meshes/pCube109_pCube124_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube110_pCube125_1
        source: "meshes/pCube110_pCube125_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube101_pCube126_1
        source: "meshes/pCube101_pCube126_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube108_pCube127_1
        source: "meshes/pCube108_pCube127_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube97_pCube128_1
        source: "meshes/pCube97_pCube128_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube131_pCube139
        source: "meshes/pCube131_pCube139.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube131_pCube139_1
        source: "meshes/pCube131_pCube139_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pSphere7
        source: "meshes/pSphere7.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pSphere8_pSphere9
        source: "meshes/pSphere8_pSphere9.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pSphere8_pSphere9_1
        source: "meshes/pSphere8_pSphere9_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pSphere10_pSphere11
        source: "meshes/pSphere10_pSphere11.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pSphere10_pSphere11_1
        source: "meshes/pSphere10_pSphere11_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube143
        source: "meshes/pCube143.mesh"
        materials: [
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material
        ]
    }


    Model {
        id: pCylinder8_pCylinder11
        source: "meshes/pCylinder8_pCylinder11.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder9_pCylinder12
        source: "meshes/pCylinder9_pCylinder12.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder10_pCylinder13
        source: "meshes/pCylinder10_pCylinder13.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder8_pCylinder11_1
        source: "meshes/pCylinder8_pCylinder11_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder9_pCylinder12_1
        source: "meshes/pCylinder9_pCylinder12_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder10_pCylinder13_1
        source: "meshes/pCylinder10_pCylinder13_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube145
        source: "meshes/pCube145.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pSphere12
        source: "meshes/pSphere12.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube146
        source: "meshes/pCube146.mesh"
        materials: [
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material
        ]
    }


    Model {
        id: polySurface17_polySurface23
        source: "meshes/polySurface17_polySurface23.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: polySurface17_polySurface23_1
        source: "meshes/polySurface17_polySurface23_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: polySurface19
        source: "meshes/polySurface19.mesh"
        materials: [
            blinn1SG_material
        ]
    }


    Model {
        id: polySurface20
        source: "meshes/polySurface20.mesh"
        materials: [
            blinn1SG_material
        ]
    }


    Model {
        id: pCylinder24_pCylinder25
        source: "meshes/pCylinder24_pCylinder25.mesh"
        materials: [
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material
        ]
    }


    Model {
        id: pCylinder24_pCylinder25_1
        source: "meshes/pCylinder24_pCylinder25_1.mesh"
        materials: [
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material
        ]
    }


    Model {
        id: pCube150
        source: "meshes/pCube150.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: polySurface26_polySurface38
        source: "meshes/polySurface26_polySurface38.mesh"
        materials: [
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material
        ]
    }


    Model {
        id: polySurface26_polySurface38_1
        source: "meshes/polySurface26_polySurface38_1.mesh"
        materials: [
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube161_pCube162
        source: "meshes/pCube161_pCube162.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube161_pCube162_1
        source: "meshes/pCube161_pCube162_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder84
        source: "meshes/pCylinder84.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder85
        source: "meshes/pCylinder85.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder86
        source: "meshes/pCylinder86.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder87
        source: "meshes/pCylinder87.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder88
        source: "meshes/pCylinder88.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder89
        source: "meshes/pCylinder89.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder90
        source: "meshes/pCylinder90.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: polySurface62
        source: "meshes/polySurface62.mesh"
        materials: [
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube163
        source: "meshes/pCube163.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube164
        source: "meshes/pCube164.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube165
        source: "meshes/pCube165.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube166
        source: "meshes/pCube166.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube169
        source: "meshes/pCube169.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube170
        source: "meshes/pCube170.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: polySurface64
        source: "meshes/polySurface64.mesh"
        materials: [
            initialShadingGroup_material,
            blinn1SG_material,
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder93
        source: "meshes/pCylinder93.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder94
        source: "meshes/pCylinder94.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder95
        source: "meshes/pCylinder95.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder96
        source: "meshes/pCylinder96.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube171
        source: "meshes/pCube171.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder97
        source: "meshes/pCylinder97.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder98
        source: "meshes/pCylinder98.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder99_pCylinder101
        source: "meshes/pCylinder99_pCylinder101.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder100_pCylinder102
        source: "meshes/pCylinder100_pCylinder102.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder99_pCylinder101_1
        source: "meshes/pCylinder99_pCylinder101_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder100_pCylinder102_1
        source: "meshes/pCylinder100_pCylinder102_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder103
        source: "meshes/pCylinder103.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder104
        source: "meshes/pCylinder104.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder105
        source: "meshes/pCylinder105.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder106
        source: "meshes/pCylinder106.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder107
        source: "meshes/pCylinder107.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder108
        source: "meshes/pCylinder108.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder109
        source: "meshes/pCylinder109.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube172
        source: "meshes/pCube172.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder111
        source: "meshes/pCylinder111.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder112
        source: "meshes/pCylinder112.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder113
        source: "meshes/pCylinder113.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube186
        source: "meshes/pCube186.mesh"

        DefaultMaterial {
            id: phong1SG_material
        }
        materials: [
            phong1SG_material
        ]
    }


    Model {
        id: pCube187
        source: "meshes/pCube187.mesh"
        materials: [
            phong1SG_material
        ]
    }


    Model {
        id: pCube188
        source: "meshes/pCube188.mesh"
        materials: [
            phong1SG_material
        ]
    }


    Model {
        id: pCube189
        source: "meshes/pCube189.mesh"
        materials: [
            phong1SG_material
        ]
    }


    Model {
        id: pCube190
        source: "meshes/pCube190.mesh"
        materials: [
            phong1SG_material
        ]
    }


    Model {
        id: pCube191
        source: "meshes/pCube191.mesh"
        materials: [
            phong1SG_material
        ]
    }


    Model {
        id: pCube192
        source: "meshes/pCube192.mesh"
        materials: [
            phong1SG_material
        ]
    }


    Model {
        id: pCube194
        source: "meshes/pCube194.mesh"
        materials: [
            phong1SG_material
        ]
    }


    Model {
        id: pCylinder115
        source: "meshes/pCylinder115.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube196
        source: "meshes/pCube196.mesh"

        DefaultMaterial {
            id: lambert2SG_material
            diffuseColor: "#ff808080"
        }
        materials: [
            lambert2SG_material
        ]
    }


    Model {
        id: pCube197
        source: "meshes/pCube197.mesh"
        materials: [
            lambert2SG_material
        ]
    }


    Model {
        id: pCube198
        source: "meshes/pCube198.mesh"
        materials: [
            lambert2SG_material
        ]
    }


    Model {
        id: polySurface81
        source: "meshes/polySurface81.mesh"

        DefaultMaterial {
            id: _initialShadingGroup_material
            diffuseColor: "#ffffff00"
        }

        DefaultMaterial {
            id: phong2SG_material
            diffuseColor: "#ffff0000"
        }
        materials: [
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            _initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            _initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong1SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            phong1SG_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material,
            initialShadingGroup_material,
            phong2SG_material
        ]
    }


    Model {
        id: _polySurface3__polySurface84__polySurface588_polySurface86_polySurface104
        source: "meshes/_polySurface3__polySurface84__polySurface588_polySurface86_polySurface104.mesh"

        DefaultMaterial {
            id: _blinn4SG_material
            diffuseColor: "#ff454545"
        }
        materials: [
            _blinn4SG_material,
            blinn1SG_material,
            initialShadingGroup_material,
            _blinn4SG_material,
            blinn1SG_material,
            initialShadingGroup_material,
            _blinn4SG_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material
        ]
    }


    Model {
        id: _polySurface3__polySurface84__polySurface588_polySurface86_polySurface104_1
        source: "meshes/_polySurface3__polySurface84__polySurface588_polySurface86_polySurface104_1.mesh"
        materials: [
            _blinn4SG_material,
            blinn1SG_material,
            initialShadingGroup_material,
            _blinn4SG_material,
            blinn1SG_material,
            initialShadingGroup_material,
            _blinn4SG_material,
            blinn1SG_material,
            initialShadingGroup_material,
            blinn1SG_material
        ]
    }


    Model {
        id: pCube272_pCube273
        source: "meshes/pCube272_pCube273.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube272_pCube273_1
        source: "meshes/pCube272_pCube273_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder118_pCylinder119
        source: "meshes/pCylinder118_pCylinder119.mesh"
        materials: [
            _initialShadingGroup_material,
            blinn2SG_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            blinn2SG_material,
            _initialShadingGroup_material
        ]
    }


    Model {
        id: pCylinder118_pCylinder119_1
        source: "meshes/pCylinder118_pCylinder119_1.mesh"
        materials: [
            _initialShadingGroup_material,
            blinn2SG_material,
            _initialShadingGroup_material,
            initialShadingGroup_material,
            blinn2SG_material,
            _initialShadingGroup_material
        ]
    }


    Model {
        id: polySurface2
        source: "meshes/polySurface2.mesh"
        materials: [
            initialShadingGroup_material,
            blinn1SG_material
        ]
    }


    Model {
        id: polySurface21
        source: "meshes/polySurface21.mesh"
        materials: [
            blinn1SG_material
        ]
    }


    Model {
        id: polySurface22
        source: "meshes/polySurface22.mesh"
        materials: [
            initialShadingGroup_material,
            blinn1SG_material
        ]
    }


    Model {
        id: polySurface105
        source: "meshes/polySurface105.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: polySurface106
        source: "meshes/polySurface106.mesh"
        materials: [
            lambert2SG_material
        ]
    }


    Model {
        id: polySurface11
        source: "meshes/polySurface11.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: polySurface12
        source: "meshes/polySurface12.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: polySurface13
        source: "meshes/polySurface13.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: polySurface14
        source: "meshes/polySurface14.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: polySurface6_polySurface9
        source: "meshes/polySurface6_polySurface9.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: polySurface6_polySurface9_1
        source: "meshes/polySurface6_polySurface9_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube275_pCube276
        source: "meshes/pCube275_pCube276.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: pCube275_pCube276_1
        source: "meshes/pCube275_pCube276_1.mesh"
        materials: [
            initialShadingGroup_material
        ]
    }


    Model {
        id: typeMesh1
        source: "meshes/typeMesh1.mesh"

        DefaultMaterial {
            id: typeBlinnSG_material
        }
        materials: [
            typeBlinnSG_material
        ]
    }
}
