import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Particles 2.2

Window {
    id: root
    width: 800
    height: 600
    visible: true
    title: qsTr("Hello World")
    color: "#000"

    property string ringImage

    Canvas{
        id: canvas
        anchors.fill: parent
        anchors.centerIn: parent
        onPaint: {
            var ctx = getContext("2d");

            var w = canvas.width;
            var h = canvas.height;
            var r = 120;
            var ox0 = w/2;
            var oy0 = h/2;
            var ox3, oy3;
            var flag = 0;

            function auxiliary() {
                 //XY轴
                 ctx.beginPath();
                 ctx.moveTo(0, oy0);
                 ctx.lineTo(w, oy0);
                 ctx.moveTo(ox0, 0);
                 ctx.lineTo(ox0, h);
                 ctx.stroke();

                 //等边三角形
                 ctx.setLineDash([5]);
                 var x23 = r * Math.cos(Math.PI/6);
                 var y23 = r * Math.sin(Math.PI/6);
                 var x24 = -x23;
                 var y24 = y23;
                 var x34 = 0;
                 var y34 = r;
                 ctx.beginPath();
                 ctx.moveTo(ox0 + x24, oy0 - y24);
                 ctx.lineTo(ox0 + x23, oy0 - y23);
                 ctx.lineTo(ox0 + x34, oy0 + y34);
                 ctx.closePath();
                 ctx.stroke();

                 ctx.beginPath();
                 ctx.moveTo(ox0, oy0);
                 ctx.lineTo(ox0 + x23, oy0 - y23);
                 ctx.moveTo(ox0, oy0);
                 ctx.lineTo(ox0 + ox3, oy0 + oy3);
                 ctx.stroke();

                 //文字
//                 ctx.fillStyle = "#000000";
//                 ctx.font="30px Verdana";
//                 ctx.fillText("2", ox0, oy0 - 1.3*r);
//                 ctx.fillText("3", ox0 + r, oy0 + r);
//                 ctx.fillText("4", ox0 - r, oy0 + r);
            }

            function draw() {
                 ctx.setLineDash([]);
                 if (flag) {
                      ctx.fillStyle = "#ffffff";
                      ctx.lineWidth = 1;
                      ctx.strokeStyle = "#000000";
                 } else {
                      ctx.lineWidth = 1;
                      ctx.strokeStyle = "#ffffff";
                 }

                 ctx.beginPath();
                 ctx.arc(ox0, oy0, r, 0, 2 * Math.PI);
                 ctx.stroke();
                 var oy2 = r * Math.cos(Math.PI/3);
                 var r2 = r * Math.sin(Math.PI/3);
                 ctx.beginPath();
                 ctx.arc(ox0, oy0 - oy2, r2, 0, 2 * Math.PI);
                 ctx.stroke();
                 var o1o3 = r * Math.sin(Math.PI/6);
                 ox3 = o1o3 * Math.cos(Math.PI/6);
                 oy3 = o1o3 * Math.sin(Math.PI/6);
                 ctx.beginPath();
                 ctx.arc(ox0 + ox3, oy0 + oy3, r2, 0, 2 * Math.PI);
                 ctx.stroke();
                 ctx.beginPath();
                 ctx.arc(ox0 - ox3, oy0 + oy3, r2, 0, 2 * Math.PI);
                 ctx.stroke();

                 //辅助线
                 if (flag) {
                      auxiliary();
                 } else {
                     //头部
                     ctx.beginPath();
                     ctx.arc(ox0, oy0 - 4*r, 2*r, Math.PI/3, Math.PI - Math.PI/3);
                     ctx.moveTo(ox0, oy0 - 2*r);
                     ctx.lineTo(ox0, oy0 + 1.5*r);
                     ctx.stroke();
                     ctx.beginPath();
                     ctx.arc(ox0, oy0 - 2*r, 3.5*r, Math.PI/3, Math.PI - Math.PI/3);
                     ctx.stroke();
                 }
            }
            draw();
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            ringImage = canvas.toDataURL()
            particles.running = true
        }
    }

    ParticleSystem {
        id: particles
        anchors.fill: parent
        running: false

        ImageParticle {
            id: smoke
            system: particles
            anchors.fill: parent
            groups: ["A"]
            source: "qrc:///particleresources/glowdot.png"
            colorVariation: 0
            color: "#00111111"
        }
        ImageParticle {
            id: flame
            anchors.fill: parent
            system: particles
            groups: ["B"]
            source: "qrc:///particleresources/glowdot.png"
            colorVariation: 0.1
            color: "#00ff400f"
        }

        Emitter {
            id: fire
            system: particles
            group: "B"

            emitRate: 1000
            lifeSpan: 2000

            acceleration: PointDirection { y: -6; yVariation: -6; xVariation: 3 }
            velocity: PointDirection {yVariation: -6}

            size: 24
            sizeVariation: 8
            endSize: 4

            //! [0]
            anchors.fill: parent
            shape: MaskShape {
                source: ringImage
            }
            //! [0]
        }

        TrailEmitter {
            id: fireSmoke
            group: "A"
            system: particles
            follow: "B"
            width: root.width
            height: root.height - 68

            emitRatePerParticle: 1
            lifeSpan: 2000

            velocity: PointDirection {y:-6; yVariation: -6; xVariation: 3}
            acceleration: PointDirection {yVariation: -6}

            size: 36
            sizeVariation: 8
            endSize: 16
        }

    }

}
