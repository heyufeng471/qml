import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Particles 2.2

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")
    Image {
        id: bg
        anchors.fill: parent
        fillMode: Image.PreserveAspectCrop
        source: "qrc:/bg1.jpg"
    }

    ParticleSystem { id: particles; anchors.fill: parent }
    Emitter {
        system: particles
        anchors.fill: parent
        emitRate: 0
        lifeSpan: 10000
        size: 24
        sizeVariation: 8
        velocity: AngleDirection { angleVariation: 360; magnitude: 30 }
        maximumEmitted: 10
        startTime: 5000
        Timer { running: true; interval: 10; onTriggered: parent.emitRate = 1; }
    }

    //! [0]
    ImageParticle {
        anchors.fill: parent
        system: particles
        source: "qrc:/star.png"
        alpha: 0.1
        color: "white"
        rotationVariation: 180
        rotationVelocity: 30
        z: 2
    }
    //! [0]

    ParticleSystem {
        anchors.fill: parent
        ImageParticle {
            groups: ["stars"]
            anchors.fill: parent
            source: "qrc:///particleresources/star.png"
        }
        Emitter {
            group: "stars"
            emitRate: 40
            lifeSpan: 2400
            size: 24
            sizeVariation: 8
            anchors.fill: parent
            velocity: AngleDirection { angleVariation: 360; magnitude: 3 }
        }
    }
}
