import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Particles 2.2

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("ImageParticle")

    Image {
        id: bg
        source: "qrc:/bg3.png"
        anchors.fill: parent
        fillMode: Image.PreserveAspectCrop
    }
    ParticleSystem {
        anchors.fill: parent

        ImageParticle {
            sprites: Sprite {
                    name: "dog"
                    source: "qrc:/sp2.png"
                    frameCount: 10
                    frameDuration: 80
                    frameWidth: 117; frameHeight: 84
                }

            entryEffect :ImageParticle.Scale
        }

        Emitter {
            emitRate: 1; lifeSpan: 9000
            velocity: PointDirection { x:-80; xVariation: 80; }
            acceleration: PointDirection { x: -40; xVariation: 40; }
            size: 120; sizeVariation: 10
            height: parent.height-400
            anchors.right: parent.right
            y: 300
            maximumEmitted: 5
        }
    }
}
