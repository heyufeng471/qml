#ifndef BINGDUNDUN_H
#define BINGDUNDUN_H

#include <QQuickPaintedItem>
#include <QQuickItem>

class BingDunDun : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName)
    Q_PROPERTY(QColor color READ color WRITE setColor)
    QML_ELEMENT
public:
    BingDunDun(QQuickItem *parent = 0);

    QString name() const;
    void setName(const QString &name);

    QColor color() const;
    void setColor(const QColor &color);

    void paint(QPainter *painter);

private:
    QString m_name;
    QColor m_color;
};

#endif // BINGDUNDUN_H
