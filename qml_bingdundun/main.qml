import QtQuick 2.15
import QtQuick.Window 2.15
import Bingdundun 1.0

Window {
    width: 640
    height: 580
    visible: true
    title: qsTr("Hello World")

    BingDunDun {
        id: bingdundun
        anchors.centerIn: parent
        anchors.fill: parent
        name: "掘友们好，我是冰墩墩"
        color: "MediumBlue"
    }

    Text {
        anchors { top: parent.top; horizontalCenter: parent.horizontalCenter; topMargin: 20 }
        font.bold: true
        font.pixelSize: 20
        font.family: "黑体"
        text: bingdundun.name
        color: bingdundun.color
    }

}
