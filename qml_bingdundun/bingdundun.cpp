#include "bingdundun.h"
#include <QPainter>
#include <QPainterPath>
#include <QtMath>


BingDunDun::BingDunDun(QQuickItem *parent)
        : QQuickPaintedItem(parent)
{

}

QString BingDunDun::name() const
{
    return m_name;
}

void BingDunDun::setName(const QString &name)
{
    m_name = name;
}

QColor BingDunDun::color() const
{
    return m_color;
}

void BingDunDun::setColor(const QColor &color)
{
    m_color = color;
}

#if 0
void BingDunDun::paint(QPainter *painter)
{
    painter->setRenderHint(QPainter::Antialiasing);

    QPainterPath path0;
    path0.moveTo(312, 270);
    path0.cubicTo(331, 284, 354, 284, 367, 271);
    painter->drawPath(path0);

    QPainterPath path1;
    path1.moveTo(312, 271);
    path1.cubicTo(333, 277, 353, 286, 367, 270);
    painter->drawPath(path1);

    painter->setPen(QPen(QColor("#2f4f4f"), 1));

    QPainterPath path2;
    path2.moveTo(251, 238);
    path2.cubicTo(253, 270, 316, 266, 296, 218);
    painter->drawPath(path2);

    QPainterPath path3;
    path3.moveTo(250, 237);
    path3.cubicTo(254, 199, 293, 212, 296, 217);
    painter->drawPath(path3);

    QPainterPath path4;
    path4.moveTo(393, 255);
    path4.cubicTo(418, 257, 438, 215, 398, 208);
    painter->drawPath(path4);

    painter->setPen(QPen(QColor("#000000"), 1));

    QPainterPath path5;
    path5.moveTo(393, 255);
    path5.cubicTo(365, 244, 372, 202, 398, 208);
    painter->drawPath(path5);

    painter->setPen(QPen(QColor("#000000"), 2));

    QPainterPath path6;
    path6.moveTo(249, 86);
    path6.cubicTo(197, 59, 152, 115, 176, 154);
    painter->drawPath(path6);

    QPainterPath path7;
    path7.moveTo(250, 88);
    path7.cubicTo(284, 64, 375, 67, 395, 81);
    painter->drawPath(path7);

    QPainterPath path8;
    path8.moveTo(395, 82);
    path8.cubicTo(417, 54, 490, 66, 458, 135);
    painter->drawPath(path8);

    QPainterPath path9;
    path9.moveTo(458, 136);
    path9.cubicTo(472, 151, 491, 174, 492, 200);
    painter->drawPath(path9);

    QPainterPath path10;
    path10.moveTo(177, 157);
    path10.cubicTo(146, 191, 140, 238, 137, 281);
    painter->drawPath(path10);

    QPainterPath path11;
    path11.moveTo(136, 283);
    path11.cubicTo(47, 389, 72, 440, 120, 423);
    painter->drawPath(path11);

    QPainterPath path12;
    path12.moveTo(120, 423);
    path12.cubicTo(151, 406, 141, 382, 149, 370);
    painter->drawPath(path12);

    QPainterPath path13;
    path13.moveTo(149, 372);
    path13.cubicTo(164, 429, 192, 421, 197, 556);
    painter->drawPath(path13);

    QPainterPath path14;
    path14.moveTo(198, 556);
    path14.cubicTo(229, 584, 300, 567, 291, 550);
    painter->drawPath(path14);

    QPainterPath path15;
    path15.moveTo(309, 519);
    path15.cubicTo(286, 515, 277, 535, 291, 549);
    painter->drawPath(path15);

    QPainterPath path16;
    path16.moveTo(310, 518);
    path16.cubicTo(320, 521, 338, 510, 329, 553);
    painter->drawPath(path16);

    QPainterPath path17;
    path17.moveTo(329, 555);
    path17.cubicTo(356, 590, 435, 573, 427, 546);
    painter->drawPath(path17);

    QPainterPath path18;
    path18.moveTo(499, 316);
    path18.cubicTo(477, 470, 431, 409, 428, 546);
    painter->drawPath(path18);

    QPainterPath path19;
    path19.moveTo(499, 315);
    path19.cubicTo(662, 212, 531, 116, 490, 202);
    painter->drawPath(path19);

    QPainterPath path20;
    path20.moveTo(207, 474);
    path20.cubicTo(220, 542, 196, 568, 279, 550);
    painter->drawPath(path20);

    QPainterPath path21;
    path21.moveTo(207, 474);
    path21.cubicTo(329, 517, 264, 516, 278, 550);
    painter->drawPath(path21);

    QPainterPath path22;
    path22.moveTo(339, 522);
    path22.cubicTo(345, 555, 369, 564, 412, 546);
    painter->drawPath(path22);

    QPainterPath path23;
    path23.moveTo(338, 520);
    path23.cubicTo(430, 472, 440, 440, 413, 547);
    painter->drawPath(path23);

    QPainterPath path24;
    path24.moveTo(139, 297);
    path24.cubicTo(47, 389, 112, 473, 147, 363);
    painter->drawPath(path24);

    QPainterPath path25;
    path25.moveTo(505, 291);
    path25.cubicTo(618, 225, 539, 124, 500, 212);
    painter->drawPath(path25);

    QPainterPath path26;
    path26.moveTo(329, 243);
    path26.cubicTo(328, 251, 358, 262, 364, 241);
    painter->drawPath(path26);

    QPainterPath path27;
    path27.moveTo(329, 243);
    path27.cubicTo(328, 226, 367, 234, 364, 242);
    painter->drawPath(path27);

    QPainterPath path28;
    path28.moveTo(500, 213);
    path28.cubicTo(500, 234, 512, 261, 504, 291);
    painter->drawPath(path28);

    QPainterPath path29;
    path29.moveTo(219, 281);
    path29.cubicTo(245, 319, 352, 239, 299, 192);
    painter->drawPath(path29);

    QPainterPath path30;
    path30.moveTo(219, 280);
    path30.cubicTo(180, 245, 255, 175, 298, 192);
    painter->drawPath(path30);

    QPainterPath path31;
    path31.moveTo(365, 219);
    path31.cubicTo(391, 292, 452, 312, 458, 273);
    painter->drawPath(path31);

    QPainterPath path32;
    path32.moveTo(364, 219);
    path32.cubicTo(379, 149, 464, 214, 456, 273);
    painter->drawPath(path32);

    QPainterPath path33;
    path33.moveTo(140, 298);
    path33.cubicTo(137, 327, 145, 347, 148, 362);
    painter->drawPath(path33);

    painter->setPen(QPen(QColor("#00ffff"), 4));

    QPainterPath path34;
    path34.moveTo(285, 121);
    path34.cubicTo(76, 208, 185, 413, 330, 404);
    painter->drawPath(path34);

    QPainterPath path35;
    path35.moveTo(289, 120);
    path35.cubicTo(523, 76, 567, 413, 333, 405);
    painter->drawPath(path35);

    painter->setPen(QPen(QColor("#6a5acd"), 4));

    QPainterPath path36;
    path36.moveTo(284, 129);
    path36.cubicTo(77, 219, 206, 415, 335, 399);
    painter->drawPath(path36);

    QPainterPath path37;
    path37.moveTo(285, 129);
    path37.cubicTo(525, 78, 548, 415, 338, 400);
    painter->drawPath(path37);

    painter->setPen(QPen(QColor("#ff4500"), 4));

    QPainterPath path38;
    path38.moveTo(189, 286);
    path38.cubicTo(254, 461, 489, 384, 476, 275);
    painter->drawPath(path38);

    QPainterPath path39;
    path39.moveTo(189, 283);
    path39.cubicTo(155, 103, 468, 62, 476, 272);
    painter->drawPath(path39);

    painter->setPen(QPen(QColor("#ffd700"), 4));

    QPainterPath path40;
    path40.moveTo(240, 344);
    path40.cubicTo(388, 451, 573, 287, 403, 154);
    painter->drawPath(path40);

    QPainterPath path41;
    path41.moveTo(238, 342);
    path41.cubicTo(100, 188, 313, 94, 400, 152);
    painter->drawPath(path41);

    painter->setPen(QPen(QColor("#adff2f"), 4));

    QPainterPath path42;
    path42.moveTo(401, 357);
    path42.cubicTo(240, 410, 134, 238, 253, 165);
    painter->drawPath(path42);

    QPainterPath path43;
    path43.moveTo(255, 164);
    path43.cubicTo(440, 70, 531, 320, 404, 357);
    painter->drawPath(path43);

}

#endif

void BingDunDun::paint(QPainter *painter)
{
    QPen pen(m_color,2);
    painter->scale(0.37,0.37);
    painter->setPen(pen);
    painter->setRenderHint(QPainter::Antialiasing);

    // 能量外壳
    QPainterPath path0;
    path0.moveTo(497,462);
    path0.cubicTo(452,380,497,184,666,297);
    path0.cubicTo(792,255,921,261,1017,278);
    path0.cubicTo(1127,155,1227,305,1183,404);
    path0.cubicTo(1208,443,1238,488,1254,544);
    path0.cubicTo(1251,421,1503,398,1472,577);
    path0.cubicTo(1407,758,1336,789,1279,876);
    path0.cubicTo(1270,924,1255,1044,1147,1222);
    path0.cubicTo(1098,1372,1211,1454,1031,1457);
    path0.cubicTo(877,1469,892,1434,901,1376);
    path0.cubicTo(924,1313,783,1324,802,1378);
    path0.cubicTo(822,1432,819,1467,691,1469);
    path0.cubicTo(571,1473,569,1448,571,1332);
    path0.cubicTo(572,1218,530,1226,464,1038);
    path0.cubicTo(386,1244,233,1115,272,1017);
    path0.cubicTo(306,916,365,845,407,777);
    path0.cubicTo(433,669,449,545,497,462);
    painter->drawPath(path0);
    painter->save();

    // 左耳
    QPainterPath path1;
    path1.moveTo(526,437);
    path1.cubicTo(498,263,667,325,641,329);
    path1.quadTo(600,343,526,437);
    painter->setBrush(m_color);
    painter->drawPath(path1);

    // 右耳
    path1.moveTo(1050,285);
    path1.cubicTo(1144,232,1167,342,1162,387);
    path1.quadTo(1119,317,1050,285);
    painter->setBrush(m_color);
    painter->drawPath(path1);

    // 左手
    QPainterPath path2;
    path2.moveTo(417,804);
    path2.cubicTo(430,837,435,914,457,968);
    path2.cubicTo(445,1016,440,1022,428,1053);
    path2.cubicTo(396,1142,307,1112,304,1048);
    path2.quadTo(300,987,418,803);
    painter->setBrush(m_color);
    painter->drawPath(path2);

    // 右手
    path2.moveTo(1267,593);
    path2.cubicTo(1275,584,1279,574,1280,555);
    path2.cubicTo(1282,448,1480,477,1429,575);
    path2.cubicTo(1403,621,1374,689,1287,757);
    path2.quadTo(1291,693,1267,594);
    painter->setBrush(m_color);
    painter->drawPath(path2);

    // 左脚
    QPainterPath path3;
    path3.moveTo(585,1231);
    path3.cubicTo(626,1261,776,1297,792,1336);
    path3.cubicTo(756,1387,838,1427,710,1428);
    path3.cubicTo(505,1431,644,1381,585,1231);
    painter->setBrush(m_color);
    painter->drawPath(path3);

    // 右脚
    path3.moveTo(910,1342);
    path3.cubicTo(981,1318,938,1293,1125,1226);
    path3.cubicTo(1087,1370,1172,1404,1014,1420);
    path3.cubicTo(875,1425,959,1403,910,1342);
    painter->setBrush(m_color);
    painter->drawPath(path3);

    // 左黑眼圈
    QPainterPath path4;
    path4.moveTo(806,552);
    path4.cubicTo(706,492,512,681,603,777);
    path4.cubicTo(738,882,896,600,806,552);
    painter->setBrush(m_color);
    painter->drawPath(path4);

    // 右黑眼圈
    path4.moveTo(989,541);
    path4.cubicTo(1080,477,1251,684,1168,768);
    path4.cubicTo(1077,837,893,607,989,541);
    painter->setBrush(m_color);
    painter->drawPath(path4);

    // 能量圈
    painter->restore();
    painter->save();
    painter->setPen(QPen(QColor("#73fd94"),7));
    QPainterPath path5;
    path5.moveTo(497,772);
    path5.cubicTo(425,371,1145,80,1262,699);
    path5.cubicTo(1294,945,1105,1031,907,1040);
    path5.cubicTo(716,1049,519,962,497,772);
    painter->drawPath(path5);

    painter->restore();
    painter->save();
    painter->setPen(QPen(QColor("#f97dfe"),5));
    QPainterPath path6;
    path6.moveTo(515,794);
    path6.cubicTo(405,421,1093,119,1242,646);
    path6.cubicTo(1316,881,1130,1001,898,1003);
    path6.cubicTo(732,1005,562,961,515,794);
    painter->drawPath(path6);

    painter->restore();
    painter->save();
    painter->setPen(QPen(QColor("#ecea87"),9));
    QPainterPath path7;
    path7.moveTo(611,909);
    path7.cubicTo(301,602,878,185,1137,487);
    path7.cubicTo(1495,981,840,1066,611,909);
    painter->drawPath(path7);

    painter->restore();
    painter->save();
    painter->setPen(QPen(QColor("#9ad6ff"),7));
    QPainterPath path8;
    path8.moveTo(611,909);
    path8.cubicTo(281,592,878,200,1137,487);
    path8.cubicTo(1495,1001,840,1076,611,909);
    painter->drawPath(path8);

    painter->restore();
    painter->save();
    painter->setPen(QPen(QColor("#9ad6ff"),5));
    QPainterPath path9;
    path9.moveTo(515,794);
    path9.cubicTo(405,421,1053,109,1242,646);
    path9.cubicTo(1316,911,1150,1001,898,1023);
    path9.cubicTo(732,1025,562,971,515,794);
    painter->drawPath(path9);

    painter->restore();
    painter->save();
    painter->setPen(QPen(QColor("#d2fbe5"),7));
    QPainterPath path10;
    path10.moveTo(545,674);
    path10.cubicTo(673,289,1265,370,1215,773);
    path10.cubicTo(1177,1083,453,1010,545,674);
    painter->drawPath(path10);

    painter->restore();
    painter->save();
    painter->setPen(QPen(QColor("#4a46be"),7));
    QPainterPath path11;
    path11.moveTo(549,752);
    path11.cubicTo(548,421,1037,320,1191,640);
    path11.cubicTo(1309,1058,597,1021,549,752);
    painter->drawPath(path11);

    painter->restore();
    painter->save();
    painter->setPen(QPen(QColor("#b5e7fe"),5));
    QPainterPath path12;
    path12.moveTo(549,752);
    path12.cubicTo(548,441,1057,300,1191,640);
    path12.cubicTo(1319,1048,567,1021,549,752);
    painter->drawPath(path12);

    // 嘴巴
    painter->restore();
    painter->save();
    painter->setPen(QPen(m_color,1));
    QPainterPath path13;
    path13.moveTo(824,728);
    path13.cubicTo(895,754,939,740,982,726);
    path13.cubicTo(935,782,861,764,824,728);
    painter->setBrush(m_color);
    painter->drawPath(path13);

    QPainterPath path14;
    path14.moveTo(870,750);
    path14.cubicTo(876,746,939,745,945,749);
    path14.cubicTo(910,764,872,755,870,750);
    painter->setBrush(QColor("#e5482d"));
    painter->drawPath(path14);

    // 小红心
    painter->restore();
    painter->save();
    QPainterPath path15;
    path15.moveTo(1364,545);
    path15.cubicTo(1359,525,1300,508,1331,595);
    path15.cubicTo(1338,615,1349,607,1356,605);
    path15.cubicTo(1394,587,1420,532,1364,545);
    painter->setBrush(QColor("#e5482d"));
    painter->drawPath(path15);

    // 左眼
    painter->restore();
    painter->save();
    QPainterPath path16;
    path16.moveTo(749,595);
    path16.cubicTo(798,592,829,709,743,712);
    path16.cubicTo(659,707,686,593,749,595);
    painter->setBrush(QColor("#ffffff"));
    painter->drawPath(path16);

    painter->restore();
    painter->save();
    QPainterPath path17;
    path17.moveTo(699,655);
    path17.cubicTo(696,596,782,574,783,653);
    path17.cubicTo(775,735,694,699,699,655);
    QRadialGradient l_eye(742,652,50,742,652,20);
    l_eye.setColorAt(0,QColor("#857343"));
    l_eye.setColorAt(1,QColor("#black"));
    painter->setBrush(l_eye);
    painter->drawPath(path17);

    painter->restore();
    painter->save();
    QPainterPath path18;
    path18.moveTo(719,655);
    path18.cubicTo(716,633,760,609,762,657);
    path18.cubicTo(755,691,723,676,719,655);
    painter->setBrush(m_color);
    painter->drawPath(path18);


    // 右眼
    painter->restore();
    painter->save();
    QPainterPath path19;
    path19.moveTo(988,630);
    path19.cubicTo(997,569,1091,548,1087,647);
    path19.cubicTo(1079,719,976,710,988,630);
    painter->setBrush(QColor("#ffffff"));
    painter->drawPath(path19);

    painter->restore();
    painter->save();
    QPainterPath path20;
    path20.moveTo(995,634);
    path20.cubicTo(993,584,1077,559,1077,641);
    path20.cubicTo(1068,707,993,689,995,634);
    QRadialGradient r_eye(1040,635,50,1040,635,20);//这里的QRadialGradient与canvas中的createRadialGradient中不太一样，前后半径需要对换
    r_eye.setColorAt(0,QColor("#857343"));
    r_eye.setColorAt(1,QColor("black"));
    painter->setBrush(r_eye);
    painter->drawPath(path20);

    painter->restore();
    painter->save();
    QPainterPath path21;
    path21.moveTo(1022,621);
    path21.cubicTo(1055,596,1065,650,1042,659);
    path21.cubicTo(1027,662,1002,646,1022,621);
    painter->setBrush(m_color);
    painter->drawPath(path21);

    // 左眼高光
    painter->restore();
    painter->save();
    painter->setBrush(QColor("#ffffff"));
    painter->drawEllipse(743,623,20,20);

    painter->restore();
    painter->save();
    painter->setBrush(QColor("#5fc2ba"));
    painter->drawEllipse(732,675,13,13);

    // 右眼高光
    painter->restore();
    painter->save();
    painter->setBrush(QColor("#ffffff"));
    painter->drawEllipse(1036,606,20,20);

    painter->restore();
    painter->save();
    painter->setBrush(QColor("#5fc2ba"));
    painter->drawEllipse(1024,659,13,13);

    // 鼻子
    painter->restore();
    painter->save();
    QPainterPath path22;
    path22.moveTo(914,646);
    path22.cubicTo(863,646,867,682,901,698);
    path22.cubicTo(920,706,927,704,941,694);
    path22.cubicTo(970,668,961,644,914,646);
    painter->setBrush(QColor("#ffffff"));
    painter->drawPath(path22);

    painter->restore();
    QPainterPath path23;
    path23.moveTo(886,666);
    path23.cubicTo(887,648,945,644,944,666);
    path23.cubicTo(944,686,886,683,886,666);
    QLinearGradient nose(910,650,910,675);
    nose.setColorAt(1,"black");
    nose.setColorAt(0,"white");
    painter->setBrush(nose);
    painter->drawPath(path23);

}

