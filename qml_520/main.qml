import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.14
import QtGraphicalEffects 1.14
import QtQuick.Particles 2.14

Window {
    visible: true
    width: 360
    height: 480
    title: qsTr("For My Lover")
    color: Qt.rgba(Math.random(),Math.random(),Math.random(),0.1)
    MouseArea{
        anchors.fill: parent
        onClicked: {
            color = Qt.rgba(Math.random(),Math.random(),Math.random(),0.1)
            particles.stop()
        }
    }
    Text {
        id: tip
        width: parent.width-parent.width/4
        height: parent.height/5
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        font.pixelSize: 24
        font.bold: true
        font.family: "黑体"
        text: qsTr("在下面的框里填上你的宝贝儿的手机号")
        horizontalAlignment: Text.AlignHCenter
        anchors.bottom: tiptmp.top
        anchors.bottomMargin: parent.height/20
        anchors.horizontalCenter: tiptmp.horizontalCenter
        color: Qt.rgba(Math.random(),Math.random(),Math.random(),1)
        MouseArea{
            anchors.fill: parent
            onClicked: {
                tip.color = Qt.rgba(Math.random(),Math.random(),Math.random(),1)
                particles.stop()
            }
        }
    }
    Text{
        id:tiptmp
        text: qsTr("(●'v'●)")
        width: parent.width-parent.width/4
        height: parent.height/5
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        font.pixelSize: 48
        font.bold: true
        font.family: "黑体"
        horizontalAlignment: Text.AlignHCenter
        anchors.centerIn: parent
        color: Qt.rgba(Math.random(),Math.random(),Math.random(),1)
        MouseArea{
            anchors.fill: parent
            onClicked: {
                tiptmp.color = Qt.rgba(Math.random(),Math.random(),Math.random(),1)
                glow.color = Qt.rgba(Math.random(),Math.random(),Math.random(),1)
            }
        }
    }
    Button{
        id: tipbutton
        text: "OK"
        font.family: "黑体"
        font.pointSize: 12
        onClicked: {
            if((!textinput.text)||(textinput.length != 11))
            {
                tip.text = "填的不对哦"
                tiptmp.text = "(●'v'●)"
            }
            else{
                tip.color = Qt.rgba(Math.random(),Math.random(),Math.random(),1)
                tip.text = "(("+textinput.text+" + 52.8)*5 - 3.9343)/0.5 -"+textinput.text+"* 10 ="
                tiptmp.text = "520.1314"
                particles.start()
            }
        }
        anchors.top: textinput.bottom
        anchors.topMargin: parent.height/20
        anchors.horizontalCenter: textinput.horizontalCenter
        background: Rectangle{
            radius: 8
            implicitWidth: 300
            implicitHeight: 45
            border.color: Qt.rgba(Math.random(),Math.random(),Math.random(),1)
            border.width: 2
        }
    }
    TextField{
        id:textinput
        anchors.top: tiptmp.bottom
        anchors.topMargin: parent.height/20
        anchors.horizontalCenter: tiptmp.horizontalCenter
        font.family: "黑体"
        font.pointSize: 12
        horizontalAlignment: Text.AlignHCenter
        validator: RegExpValidator{regExp: /[0-9]{11}/}
        background: Rectangle{
            radius: 8
            implicitWidth: 300
            implicitHeight: 45
            border.color: Qt.rgba(Math.random(),Math.random(),Math.random(),1)
            border.width: 2
        }
    }
    Glow{
        id:glow
        anchors.fill: tiptmp
        source: tiptmp
        radius: 16
        samples: 24
        color: Qt.rgba(Math.random(),Math.random(),Math.random(),0.5)
        spread: 0.5
    }

    ParticleSystem { id: particles; running: false}
    ImageParticle{
        system: particles
        source: "qrc:/heart.png"
    }
    Emitter {
        system: particles;
        x: 0;
        width: parent.width
        lifeSpan : 1000
        lifeSpanVariation : 1000
        maximumEmitted : 10
        velocity: PointDirection {y:200; yVariation: 100; }
        size: 64
        endSize: 72
        sizeVariation: 48
    }
}
